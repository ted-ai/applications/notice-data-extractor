# TED AI - Notice data extractor

## Description

This project deploys a Step Function pipeline that reads all the XML notices from S3 input bucket in order to extract
business valuable information to create a specific dataset stored in the curated S3 bucket.

This curated S3 bucket data is then referenced by a Glue table in the Glue Data Catalog that can be queried by the
business users via Amazon Athena service.

![Data extract pipeline](./doc/data-extraction-pipeline.png)

## Prerequisites

- Python 3.10.9
- Node 19.9.0
- npm 9.6.4

## Install serverless 

```shell
npm install -g serverless
```

## Install python requirements in a virtual environment

```shell
python -m venv venv
source ./venv/bin/activate
pip install -r requirements-local.txt
```

## Deploy from the local environment

1. Set an AWS profile having the rights to deploy in the environment and set the correct region

```shell
export AWS_PROFILE=tedai-dev
export AWS_REGION=eu-west-1
```

2. Install the required plugins

```shell
sudo serverless plugin install -n serverless-python-requirements --stage <stage>
sudo serverless plugin install -n serverless-step-functions --stage <stage>
sudo serverless plugin install -n serverless-plugin-utils --stage <stage>
sudo serverless plugin install -n serverless-deployment-bucket --stage <stage>
```

3. Deploy the serverless project

```shell
serverless deploy --stage <stage>
```

## Run parsing locally

It is possible to run the parsing on a local XML notice with the following command:
```shell
LOGGER_NAME=logger python scripts/process_notice.py --path data/notices/can.xml --type contract_award_notice
```
