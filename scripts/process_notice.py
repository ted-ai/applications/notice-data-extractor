import argparse
import json
import sys
from dataclasses import dataclass
from pathlib import Path

sys.path.append(str(Path(__file__).parent.parent))

from src.lambdas.download_resources import notice_service

S3_KEY = "resource_type={type}/format=xml/year=0/month=0/day=0/contract_id=0/000000-0000.xml"


@dataclass
class Args:
    path: Path
    type: Path


def parse_args():
    parser = argparse.ArgumentParser(description="Run notice parsing locally")
    parser.add_argument("--path", type=str, help="Notice path")
    parser.add_argument("--type", type=str, help="Notice type")
    args = parser.parse_args()
    return Args(path=Path(args.path), type=args.type)


def main():
    args = parse_args()
    print(f"Args: {args}")
    notice_str = args.path.read_text()
    print(f"Notice read from {args.path}")
    notice = notice_service.process_notice(notice_str, "bucket", S3_KEY.format(type=args.type))
    print(f"Extracted info: {json.dumps(notice, indent=4)}")


if __name__ == '__main__':
    main()
