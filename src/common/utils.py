import os
from typing import Any, List

from lxml.etree import _Element

from src.common.config import CHANGE_NOTICE_LABELS
from src.common.config import CONTRACT_AWARD_NOTICE_LABELS
from src.common.config import CONTRACT_NOTICE_LABELS
from src.common.config import PRIOR_INFORMATION_NOTICE_LABELS
from src.common.exceptions import MissingEnvironmentVariableException
from src.common.models import NoticeType

INVALID_KEY_CHAR = ['@', '#']


def get_env_var_value(env_variable_name: str) -> str:
    try:
        return os.environ[env_variable_name]
    except KeyError as err:
        raise MissingEnvironmentVariableException(
                f"Missing environment variable '{env_variable_name}', process stopped.") from err


def normalize_notice_id(notice_id: str) -> str:
    if notice_id:
        notice_id = notice_id.strip()
        if '/' in notice_id:
            year = notice_id.split('/')[0]
            number = notice_id.split('-')[-1].lstrip('0')
        else:
            split = notice_id.split('-')
            number = split[0].lstrip('0')
            year = split[1]
        return f'{number}-{year}'
    return ''


def clean_str(input_string: str) -> str:
    str_en = input_string.encode("ascii", "ignore")
    str_de = str_en.decode()
    invalid_chars = ['—', '-', '\'', ',', '’', '–', '"', '|', '_', '`', '[', ']', '\n']
    for char in invalid_chars:
        str_de = str_de.replace(char, '')
    return ' '.join(str_de.split())


def is_valid_etendering_url(url):
    return url and all(elt in url for elt in ['etendering', 'https'])


def normalize_notice_type(raw_type: str) -> NoticeType:
    if any(label == raw_type for label in PRIOR_INFORMATION_NOTICE_LABELS):
        return NoticeType.PRIOR_INFORMATION_NOTICE
    if any(label == raw_type for label in CONTRACT_NOTICE_LABELS):
        return NoticeType.CONTRACT_NOTICE
    if any(label == raw_type for label in CONTRACT_AWARD_NOTICE_LABELS):
        return NoticeType.CONTRACT_AWARD_NOTICE
    if any(label == raw_type for label in CHANGE_NOTICE_LABELS):
        return NoticeType.CHANGE_NOTICE
    return NoticeType.OTHER


def get_field_from_xpath(xml: _Element, namespaces: dict, xpath: str) -> str:
    attribute = xml.xpath(xpath, namespaces=namespaces)
    return clean_str(attribute[0]) if len(attribute) else ""


def run_xpath(xml: _Element, xpath: str, namespaces: dict) -> list[Any]:
    try:
        return xml.xpath(xpath, namespaces=namespaces)
    except Exception as e:
        print(e)
        print(f"xpath: {xpath}")
        exit()


def first_xpath_result(xml: _Element, xpath: str, namespaces: dict) -> str:
    return next(iter(run_xpath(xml, xpath, namespaces)), "")


def first_from_multiple_xpath_result(xml: _Element, xpaths: list[str], namespaces: dict) -> str:
    for xpath in xpaths:
        result = next(iter(run_xpath(xml, xpath, namespaces)), '')
        if result:
            return str(result)
    return ''


def batches(elements: List, size: int) -> List:
    n = len(elements)
    for ndx in range(0, n, size):
        yield elements[ndx:min(ndx + size, n)]
