SSM_S3_INPUT_BUCKET_ID = '/tedai/s3/input_bucket/id'
SSM_S3_CURATED_BUCKET_ID = '/tedai/s3/curated_bucket/id'
SQS_MAX_BATCH_SIZE = 10  # max currently allowed by AWS
PRIOR_INFORMATION_NOTICE_LABELS = [
    "Call for expressions of interest",
    "Prior Information Notice",
    "Prior information notice without call for competition",
    "Prior information notice with call for competition",
    "Periodic indicative notice with call for competition",
    "pin-cfc-standard",
    "pin-tran",
    "pin-cfc-social",
    "pin-rtl",
    "pin-only"
]
CONTRACT_NOTICE_LABELS = [
    "Contract notice",
    "Prequalification notices",
    "General information",
    "Public works concession",
    "Services concession",
    "Request for proposals",
    "Not applicable",
    "Dynamic purchasing system",
    "cn-social",
    "cn-standard"
]
CONTRACT_AWARD_NOTICE_LABELS = [
    "Contract award",
    "Contract award notice",
    "Concession award notice",
    "can-standard",
    "can-social",
    "can-tran",
]
CHANGE_NOTICE_LABELS = [
    "Additional information",
    "Corrigenda",
    "Corrigendum",
    "Modification of a contract/concession during its term",
    "can-modif",
    "corr"
]