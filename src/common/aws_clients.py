import boto3


def s3_client():
    return boto3.client("s3")


def ssm_client():
    return boto3.client('ssm')


def sqs_client():
    return boto3.client('sqs')
