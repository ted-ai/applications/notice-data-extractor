import logging
import uuid
from typing import List

from src.common.aws_clients import s3_client
from src.common.aws_clients import sqs_client
from src.common.aws_clients import ssm_client
from src.common.config import SQS_MAX_BATCH_SIZE
from src.common.exceptions import GetSSMException, S3ReadException, S3DeleteException
from src.common.exceptions import SqsInsertionException
from src.common.utils import get_env_var_value, batches


def get_ssm_parameter(path: str) -> str:
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    try:
        parameter = ssm_client().get_parameter(Name=path)
        table_name = parameter['Parameter']['Value']
        logger.info(f"Parameter : '{path}' retrieved from SSM")
        return table_name
    except Exception as err:
        raise GetSSMException(f"Get SSM Exception when retrieving parameter : '{path}'."
                              f" Exception : {err}") from err


def insert_in_queue(entries: List[dict], queue_url: str) -> None:
    try:
        sqs_client().send_message_batch(QueueUrl=queue_url, Entries=entries)
    except Exception as err:
        raise SqsInsertionException(f"Error while inserting in the SQS queue: {err}") from err


def insert_batches_in_queue(resources: List[str], queue_url: str, bucket: str) -> None:
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    batches = [resources[x:x + SQS_MAX_BATCH_SIZE] for x in range(0, len(resources), SQS_MAX_BATCH_SIZE)]
    logger.info(f'Inserting {len(resources)} in SQS queue : {queue_url}')
    for batch in batches:
        entries = []
        for resource_id in batch:
            entry = {'Id': str(uuid.uuid4()), 'MessageBody': f"{bucket}#{resource_id}"}
            entries.append(entry)
        insert_in_queue(entries, queue_url)


def get_queue_messages_status():
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    queue_url = get_env_var_value("QUEUE_URL")
    response = sqs_client().get_queue_attributes(
        QueueUrl=queue_url,
        AttributeNames=[
            'ApproximateNumberOfMessages',
            'ApproximateNumberOfMessagesNotVisible',
            'ApproximateNumberOfMessagesDelayed'
        ]
    )
    attributes = response.get("Attributes", {})
    if attributes:
        logger.info(f"Retrieved the following queue attributes: {attributes}")
    else:
        logger.info(f"No attributes returned from {queue_url}")
    return attributes


def get_notice_from_s3(bucket: str, key: str):
    obj = s3_client().get_object(Bucket=bucket, Key=key)
    return obj['Body'].read().decode('utf-8')


def get_s3_objects_list(bucket: str, prefix: str = '') -> List[str]:
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    paths = list()

    try:
        s3 = s3_client()
        paginator = s3.get_paginator('list_objects_v2')
        pages = paginator.paginate(Bucket=bucket, Prefix=prefix)

        logger.info(f'Started retrieving objects from S3 with pagination')
        page_count = 1
        for page in pages:
            logger.info(f'Processing page {page_count}')
            s3_objects = page.get('Contents', list())
            page_object_count = 0
            for s3_object in s3_objects:
                paths.append(s3_object['Key'])
                page_object_count += 1
            logger.info(f'Retrieved {page_object_count} from page {page_count}')
            page_count += 1
        logger.info(f"Finished retrieving {len(paths)} S3 objects paths.")
        return paths
    except Exception as err:
        raise S3ReadException(f"Failed to read S3 object: {err}") from err


def delete_s3_objects(bucket: str, keys: List[str]) -> None:
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    batch_size = 100
    for batch in batches(keys, batch_size):
        logger.info(f'Deleting batch: {batch}')
        try:
            response = s3_client().delete_objects(
                Bucket=bucket,
                Delete={
                    'Objects': [
                        {
                            'Key': key
                        } for key in batch
                    ]
                }
            )
            logger.info(f'Objects deleted, response: {response}')
        except Exception as err:
            raise S3DeleteException(f"Failed to delete object: {err}") from err
