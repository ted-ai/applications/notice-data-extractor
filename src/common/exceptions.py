class MissingEnvironmentVariableException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class GetSSMException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class SqsInsertionException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class S3ReadException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class S3UploadException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class S3DeleteException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message

