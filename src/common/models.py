from dataclasses import dataclass
from enum import Enum
from typing import List


class ResourceType(Enum):
    NOTICE = "notice"
    PROCUREMENT = "procurement"


class NoticeType(Enum):
    PRIOR_INFORMATION_NOTICE = "prior_information_notice"
    CONTRACT_NOTICE = "contract_notice"
    CONTRACT_AWARD_NOTICE = "contract_award_notice"
    CHANGE_NOTICE = "change_notice"
    OTHER = "other"


@dataclass
class Notice:
    id: str
    type: str
    title: str
    short_description: str
    contract_type: str
    main_cpv: str
    additional_cpvs: str
    publication_date: str
    reference_notice_id: str
    version: str
    etendering_url: str
    awarded_companies: List[dict]
    budgetary_values: List[dict]
    buyer: dict
    country: str
    language: str
    is_eu_institution: bool


@dataclass
class AwardedCompany:
    name: str
    national_id: str
    town: str
    address: str
    postal_code: str
    country: str
    phone: str
    email: str
    website: str


@dataclass
class BudgetaryValues:
    type: str
    lot: str
    estimated_value: str
    estimated_value_currency: str
    min_value: str
    min_value_currency: str
    max_value: str
    max_value_currency: str


@dataclass
class Buyer:
    name: str
    address: str
    town: str
    postal_code: str