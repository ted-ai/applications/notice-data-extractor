NOTICE_ID_XPATH = "//efbc:NoticePublicationID/text()"
LANGUAGE_XPATH = "//cbc:NoticeLanguageCode/text()"
NOTICE_TYPE_XPATH = "//cbc:NoticeTypeCode/text()"
SHORT_DESC_XPATH = [
    "//cac:ProcurementProject/cbc:Description/text()"
]
CONTRACT_TYPE_XPATH = "//cbc:ProcurementTypeCode/text()"
MAIN_CPV_XPATH = [
    "//cac:MainCommodityClassification//cbc:ItemClassificationCode/text()"
]
NOTICE_TITLE_XPATH = [
    "//cac:ProcurementProject/cbc:Name/text()"
]
ADDITIONAL_CPVS_XPATH = ("//cac:ProcurementProjectLot/cac:ProcurementProject/cac:AdditionalCommodityClassification/cbc"
                         ":ItemClassificationCode/text()")
PUB_DATE_XPATH = "//efbc:PublicationDate/text()"
REF_NOTICE_ID_XPATH = "//cac:NoticeDocumentReference/cbc:ID/text()"
NOTICE_VERSION_XPATH = "//cbc:CustomizationID/text()"
ETENDERING_URL_XPATH = "//cbc:AccessToolsURI/text()"
LOTS_XPATH = "//efac:LotResult/efac:TenderLot/cbc:ID/text()"
TENDERING_PARTY_XPATH = "//efac:LotTender/efac:TenderLot[cbc:ID='{}']/..//efac:TenderingParty/cbc:ID/text()"
ORGANIZATION_XPATH = "//efac:TenderingParty[cbc:ID='{}']/..//efac:Tenderer/cbc:ID/text()"
COMPANY_XPATH_PREFIX = "//efac:Organization/efac:Company/cac:PartyIdentification[cbc:ID='{}']/.."
COMPANY_NAME_XPATH = COMPANY_XPATH_PREFIX + "//cac:PartyName/cbc:Name/text()"
COMPANY_NATIONAL_ID_XPATH = COMPANY_XPATH_PREFIX + "//cac:PartyLegalEntity/cbc:CompanyID/text()"
COMPANY_TOWN_XPATH = COMPANY_XPATH_PREFIX + "//cac:PostalAddress/cbc:CityName/text()"
COMPANY_ADDRESS_XPATH = COMPANY_XPATH_PREFIX + "//cac:PostalAddress/cbc:StreetName/text()"
COMPANY_POSTAL_CODE_XPATH = COMPANY_XPATH_PREFIX + "//cac:PostalAddress/cbc:Country/cbc:IdentificationCode[" \
                                                   "@listName='country']/text()"
COMPANY_COUNTRY_XPATH = COMPANY_XPATH_PREFIX + "//cac:PostalAddress/cbc:CityName/text()"
COMPANY_PHONE_XPATH = COMPANY_XPATH_PREFIX + "//cac:Contact/cbc:Telephone/text()"
COMPANY_EMAIL_XPATH = COMPANY_XPATH_PREFIX + "//cac:Contact/cbc:ElectronicMail/text()"
ESTIMATED_VALUE_XPATH = "./*/cac:ProcurementProject/cac:RequestedTenderTotal/cbc:EstimatedOverallContractAmount/text()"
ESTIMATED_VALUE_CURRENCY_XPATH = ("./*/cac:ProcurementProject/cac:RequestedTenderTotal/cbc"
                                  ":EstimatedOverallContractAmount/@currencyID")
MIN_VALUE_XPATH = "./*/cac:ProcurementProject/cac:RequestedTenderTotal//efbc:FrameworkMinimumAmount/text()"
MIN_VALUE_CURRENCY_XPATH = ("./*/cac:ProcurementProject/cac:RequestedTenderTotal//efbc:FrameworkMinimumAmount"
                            "/@currencyID")
MAX_VALUE_XPATH = "./*/cac:ProcurementProject/cac:RequestedTenderTotal//efbc:FrameworkMaximumAmount/text()"
MAX_VALUE_CURRENCY_XPATH = ("./*/cac:ProcurementProject/cac:RequestedTenderTotal//efbc:FrameworkMaximumAmount"
                            "/@currencyID")
LOT_ESTIMATED_VALUE_XPATH = ("//cac:ProcurementProjectLot/cbc:ID[{"
                             "lot}]/..//cac:RequestedTenderTotal/cbc:EstimatedOverallContractAmount/text()")
LOT_ESTIMATED_VALUE_CURRENCY_XPATH = ("//cac:ProcurementProjectLot/cbc:ID[{"
                                      "lot}]/..//cac:RequestedTenderTotal/cbc:EstimatedOverallContractAmount"
                                      "/@currencyID")
LOT_MIN_VALUE_XPATH = ("//cac:ProcurementProjectLot/cbc:ID[{"
                       "lot}]/..//cac:RequestedTenderTotal//efbc:FrameworkMinimumAmount/text()")
LOT_MIN_VALUE_CURRENCY_XPATH = ("//cac:ProcurementProjectLot/cbc:ID[{"
                                "lot}]/..//cac:RequestedTenderTotal//efbc:FrameworkMinimumAmount/@currencyID")
LOT_MAX_VALUE_XPATH = ("//cac:ProcurementProjectLot/cbc:ID[{"
                       "lot}]/..//cac:RequestedTenderTotal//efbc:FrameworkMaximumAmount/text()")
LOT_MAX_VALUE_CURRENCY_XPATH = ("//cac:ProcurementProjectLot/cbc:ID[{"
                                "lot}]/..//cac:RequestedTenderTotal//efbc:FrameworkMaximumAmount/@currencyID")
BUYER_NAME_XPATH = "//efac:Organizations/efac:Organization[1]/efac:Company/cac:PartyName/cbc:Name/text()"
BUYER_ADDRESS_XPATH = "//efac:Organizations/efac:Organization[1]/efac:Company/cac:PostalAddress/cbc:StreetName/text()"
BUYER_TOWN_XPATH = "//efac:Organizations/efac:Organization[1]/efac:Company/cac:PostalAddress/cbc:CityName/text()"
BUYER_POSTAL_CODE_XPATH = ("//efac:Organizations/efac:Organization["
                           "1]/efac:Company/cac:PostalAddress/cbc:PostalZone/text()")
COUNTRY_ISO_3_XPATH = ("//efac:Organizations/efac:Organization["
                       "1]/efac:Company/cac:PostalAddress/cac:Country/cbc:IdentificationCode['country']/text()")
BUYER_TYPE_XPATH = ("//cac:ContractingParty/cac:ContractingPartyType/cbc:PartyTypeCode["
                    "@listName='buyer-legal-type']/text()")
