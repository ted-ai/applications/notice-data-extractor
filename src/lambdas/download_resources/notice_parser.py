import logging
import os
from dataclasses import asdict
from pprint import pprint
from typing import List
from typing import Union

from lxml import etree
from lxml.etree import _Element

from src.common import utils
from src.common.country_convertion import convert_to_3_letter_code
from src.common.models import AwardedCompany
from src.common.models import BudgetaryValues
from src.common.models import Buyer
from src.common.models import Notice
from src.common.models import NoticeType
from src.common.utils import first_from_multiple_xpath_result
from src.common.utils import first_xpath_result
from src.common.utils import get_field_from_xpath
from src.common.utils import is_valid_etendering_url
from src.common.utils import normalize_notice_id, clean_str
from src.common.utils import normalize_notice_type
from src.common.utils import run_xpath
from src.common.notice_xpaths import *


class NoticeParser:
    xml: _Element
    namespaces: dict

    def __init__(self, xml: _Element):
        self.xml = xml
        self.namespaces = {'x': xml.nsmap[None]}
        self.logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))

    def parse(self) -> dict:
        notice_id = self.get_notice_id()
        notice_type = self.get_notice_type()
        language = "EN"
        notice_title = self.get_title(language)
        notice_short_description = self.get_short_description(language)
        official_language = self.get_language()
        if not notice_title and not notice_short_description:
            language = official_language
            notice_title = self.get_title(official_language)
            notice_short_description = self.get_short_description(official_language)
        notice_contract_type = self.get_contract_type()
        notice_main_cpv = self.get_main_cpv(official_language)
        notice_additional_cpvs = self.get_additional_cpvs()
        if notice_additional_cpvs:
            if notice_main_cpv and notice_main_cpv in notice_additional_cpvs:
                notice_additional_cpvs.remove(notice_main_cpv)
            notice_additional_cpvs = list(set(notice_additional_cpvs))
            notice_additional_cpvs = ",".join(notice_additional_cpvs)
        else:
            notice_additional_cpvs = ""
        notice_publication_date = self.get_pub_date()
        notice_reference_notice_id = self.get_ref_notice_id()
        notice_version = self.get_notice_version()
        etendering_url = self.get_etendering_url()
        awarded_companies = []
        if notice_type == NoticeType.CONTRACT_AWARD_NOTICE:
            awarded_companies = self.get_awarded_companies(notice_version)
        budgetary_values = self.get_budgetary_values()
        buyer = self.get_buyer(official_language)
        country = self.get_country()
        is_eu_institution = self.is_eu_institution()
        notice = Notice(
                id=notice_id,
                type=notice_type.value,
                title=notice_title,
                short_description=notice_short_description,
                contract_type=notice_contract_type,
                main_cpv=notice_main_cpv,
                additional_cpvs=notice_additional_cpvs,
                publication_date=notice_publication_date,
                reference_notice_id=notice_reference_notice_id,
                version=notice_version,
                etendering_url=etendering_url,
                awarded_companies=awarded_companies,
                budgetary_values=budgetary_values,
                buyer=buyer,
                country=country,
                language=language,
                is_eu_institution=is_eu_institution
        )
        self.logger.info(f'Finished parsing the notice.')
        return asdict(notice)

    def get_title(self, language: str) -> str:
        self.logger.info(f"[notice parser] Retrieving title with XPATH: {NOTICE_TITLE_XPATH} with language '{language}'")
        title = first_xpath_result(self.xml, NOTICE_TITLE_XPATH.format(lang=language), self.namespaces)
        return clean_str(title)

    def get_short_description(self, language: str) -> str:
        self.logger.info(f"[notice parser] Retrieving short description with XPATH: {SHORT_DESC_XPATH} with language '{language}'")
        desc = first_xpath_result(self.xml, SHORT_DESC_XPATH.format(lang=language), self.namespaces)
        return clean_str(desc)

    def get_notice_id(self) -> str:
        self.logger.info(f"[notice parser] Retrieving notice ID with XPATH: {NOTICE_ID_XPATH}")
        notice_id = first_xpath_result(self.xml, NOTICE_ID_XPATH, self.namespaces)
        return normalize_notice_id(notice_id)

    def get_pub_date(self) -> str:
        self.logger.info(f"[notice parser] Retrieving publication date with XPATH: {PUB_DATE_XPATH}")
        date = first_xpath_result(self.xml, PUB_DATE_XPATH, self.namespaces)
        return date

    def get_ref_notice_id(self) -> str:
        self.logger.info(f"[notice parser] Retrieving reference notice ID with XPATH: {REF_NOTICE_ID_XPATH}")
        ref = first_xpath_result(self.xml, REF_NOTICE_ID_XPATH, self.namespaces)
        return normalize_notice_id(ref)

    def get_main_cpv(self, language: str) -> str:
        self.logger.info(f"[notice parser] Retrieving main CPV with XPATH: {MAIN_CPV_XPATH}")
        cpv = first_xpath_result(self.xml, MAIN_CPV_XPATH.format(lang="EN"), self.namespaces)
        if not cpv:
            cpv = first_xpath_result(self.xml, MAIN_CPV_XPATH.format(lang=language), self.namespaces)
        return str(cpv)

    def get_additional_cpvs(self) -> List[str]:
        self.logger.info(f"[notice parser] Retrieving additional CPVs with XPATH: {ADDITIONAL_CPVS_XPATH}")
        cpvs = run_xpath(self.xml, ADDITIONAL_CPVS_XPATH, self.namespaces)
        return cpvs

    def get_contract_type(self) -> str:
        self.logger.info(f"[notice parser] Retrieving contract type with XPATH: {CONTRACT_TYPE_XPATH}")
        contract_type = first_xpath_result(self.xml, CONTRACT_TYPE_XPATH, self.namespaces)
        return str(contract_type)

    def get_notice_version(self) -> str:
        self.logger.info(f"[notice parser] Retrieving notice version with XPATH: {NOTICE_VERSION_XPATH}")
        version = first_from_multiple_xpath_result(self.xml, NOTICE_VERSION_XPATH, self.namespaces)
        return version

    def get_notice_type(self) -> NoticeType:
        self.logger.info(f"[notice parser] Retrieving notice type with XPATH: {NOTICE_TYPE_XPATH}")
        notice_type = first_xpath_result(self.xml, NOTICE_TYPE_XPATH, self.namespaces)
        return normalize_notice_type(notice_type)

    def get_etendering_url(self) -> str:
        self.logger.info(f"[notice parser] Retrieving etendring URL with XPATH: {ETENDERING_URL_XPATH}")
        url_result = first_xpath_result(self.xml, ETENDERING_URL_XPATH, self.namespaces)
        return url_result if is_valid_etendering_url(url_result) else ''

    def get_awarded_companies(self, version: str) -> Union[List[dict], str]:
        self.logger.info(f"[notice parser] Retrieving awarded companies")
        if unsuccessful_procurement(self.xml, self.namespaces):
            return []
        if "R2.0.9" in version:
            return get_awarded_companies_r209(self.xml, self.namespaces)
        elif "R2.0.8" in version:
            return get_awarded_companies_r208(self.xml, self.namespaces)
        return []

    def get_budgetary_values(self) -> List[dict]:
        self.logger.info(f"[notice parser] Retrieving budgetary values")
        values = [asdict(BudgetaryValues(
                type="PROCUREMENT",
                lot="",
                estimated_value=first_xpath_result(self.xml, ESTIMATED_VALUE_XPATH, self.namespaces),
                estimated_value_currency=first_xpath_result(self.xml, ESTIMATED_VALUE_CURRENCY_XPATH, self.namespaces),
                min_value=first_xpath_result(self.xml, MIN_VALUE_XPATH, self.namespaces),
                min_value_currency=first_xpath_result(self.xml, MIN_VALUE_CURRENCY_XPATH, self.namespaces),
                max_value=first_xpath_result(self.xml, MAX_VALUE_XPATH, self.namespaces),
                max_value_currency=first_xpath_result(self.xml, MAX_VALUE_CURRENCY_XPATH, self.namespaces)
        ))]
        for lot in set(next(filter(None, (self.xml.xpath(BUDGET_LOTS_XPATH.format(lang=lang), namespaces=self.namespaces) for lang in LANGUAGES)), [])):
            clean_lot = "concat('" + lot.replace("'", "', \"'\", '") + "')" if lot.count("'") else "'" + lot + "'"
            values.append(asdict(BudgetaryValues(
                type="LOT",
                lot=clean_lot,
                estimated_value=first_xpath_result(self.xml, LOT_ESTIMATED_VALUE_XPATH.format(lot=clean_lot), self.namespaces),
                estimated_value_currency=first_xpath_result(self.xml, LOT_ESTIMATED_VALUE_CURRENCY_XPATH.format(lot=clean_lot), self.namespaces),
                min_value=first_xpath_result(self.xml, LOT_MIN_VALUE_XPATH.format(lot=clean_lot), self.namespaces),
                min_value_currency=first_xpath_result(self.xml, LOT_MIN_VALUE_CURRENCY_XPATH.format(lot=clean_lot), self.namespaces),
                max_value=first_xpath_result(self.xml, LOT_MAX_VALUE_XPATH.format(lot=clean_lot), self.namespaces),
                max_value_currency=first_xpath_result(self.xml, LOT_MAX_VALUE_CURRENCY_XPATH.format(lot=clean_lot), self.namespaces)
            )))
        return values

    def get_buyer(self, language: str) -> dict:
        self.logger.info(f"[notice parser] Retrieving buyer")
        buyer_name = first_xpath_result(self.xml, BUYER_NAME_XPATH.format(lang="EN"), self.namespaces)
        if not buyer_name:
            buyer_name = first_xpath_result(self.xml, BUYER_NAME_XPATH.format(lang=language),
                                                          self.namespaces)

        buyer = Buyer(
                name=buyer_name,
                address=first_from_multiple_xpath_result(self.xml, BUYER_ADDRESS_XPATH, self.namespaces),
                town=first_from_multiple_xpath_result(self.xml, BUYER_TOWN_XPATH, self.namespaces),
                postal_code=first_from_multiple_xpath_result(self.xml, BUYER_POSTAL_CODE_XPATH, self.namespaces)
        )
        return asdict(buyer)

    def get_country(self) -> str:
        self.logger.info(f"[notice parser] Retrieving country with XPATH: {COUNTRY_ISO_2_XPATH}")
        country = first_from_multiple_xpath_result(self.xml, COUNTRY_ISO_2_XPATH, self.namespaces)
        return convert_to_3_letter_code(country)

    def get_language(self) -> str:
        self.logger.info(f"[notice parser] Retrieving language with XPATH: {LANGUAGE_NOTICE_XPATH}")
        language = first_xpath_result(self.xml, LANGUAGE_NOTICE_XPATH, self.namespaces)
        return language

    def is_eu_institution(self) -> bool:
        self.logger.info(f"[notice parser] Retrieving is_eu_institution with XPATH: {BUYER_TYPE_XPATH}")
        buyer_type = first_xpath_result(self.xml, BUYER_TYPE_XPATH, self.namespaces)
        regulation_code = first_xpath_result(self.xml, REGULATION_CODE_XPATH, self.namespaces)
        return regulation_code == "3" or buyer_type == "EU_INSTITUTION"


def unsuccessful_procurement(xml: _Element, namespaces: dict):
    unsuccessful_labels = xml.xpath(UNSUCCESSFUL_PROCUREMENT_XPATH, namespaces=namespaces)
    discontinued_labels = xml.xpath(DISCONTINUED_PROCUREMENT_XPATH, namespaces=namespaces)
    return len(unsuccessful_labels) or len(discontinued_labels)


def get_awarded_company_info(company_xml: _Element, namespaces: dict) -> dict:
    awarded_company = AwardedCompany(
            get_field_from_xpath(company_xml, namespaces, COMPANY_NAME_XPATH),
            get_field_from_xpath(company_xml, namespaces, COMPANY_NATIONAL_ID_XPATH),
            get_field_from_xpath(company_xml, namespaces, COMPANY_TOWN_XPATH),
            get_field_from_xpath(company_xml, namespaces, COMPANY_ADDRESS_XPATH),
            get_field_from_xpath(company_xml, namespaces, COMPANY_POSTAL_CODE_XPATH),
            get_field_from_xpath(company_xml, namespaces, COMPANY_COUNTRY_XPATH),
            get_field_from_xpath(company_xml, namespaces, COMPANY_PHONE_XPATH),
            get_field_from_xpath(company_xml, namespaces, COMPANY_EMAIL_XPATH),
            get_field_from_xpath(company_xml, namespaces, COMPANY_WEBSITE_XPATH)
    )
    return asdict(awarded_company)


def get_lot_number(xml: _Element, namespaces: dict) -> int:
    lot_number = xml.xpath(".//x:LOT_NUMBER/text()", namespaces=namespaces)
    return int(lot_number[0]) if len(lot_number) else 1


def get_awarded_companies_r208(xml: _Element, namespaces: dict) -> List[dict]:
    result = []
    lots = xml.xpath(LOT_R208_XPATH, namespaces=namespaces)
    if len(lots):
        for lot in lots:
            awarded_companies_result = []
            lot_number = get_lot_number(lot, namespaces)
            awarded_companies_xml = lot.xpath(AWARDED_COMPANIES_R208_XPATH, namespaces=namespaces)
            for company_xml in awarded_companies_xml:
                awarded_company = get_awarded_company_info(company_xml, namespaces)
                awarded_companies_result.append(awarded_company)
            lot_result = {
                "lot": str(lot_number),
                "awarded": awarded_companies_result
            }
            result.append(lot_result)
        return result


def get_awarded_companies_r209(xml: _Element, namespaces: dict) -> List[dict]:
    result = []
    for xpath in LOT_R209_XPATH:
        lot_number = 1
        lots = xml.xpath(xpath, namespaces=namespaces)
        for lot in lots:
            awarded_companies_result = []
            awarded_companies_xml = lot.xpath(AWARDED_COMPANIES_R209_XPATH, namespaces=namespaces)
            for company_xml in awarded_companies_xml:
                awarded_company = get_awarded_company_info(company_xml, namespaces)
                awarded_companies_result.append(awarded_company)
            lot_result = {
                "lot": str(lot_number),
                "awarded": awarded_companies_result
            }
            result.append(lot_result)
            lot_number += 1
    return result


if __name__ == '__main__':
    os.environ["LOGGER_NAME"] = "notice_parser"
    print(f"Contract notice:")
    with open('../../../data/notices/contract_notice.xml') as f:
        content = f.read()
        xml = etree.fromstring(bytes(content, encoding='utf-8'))
        print(f"Extracted the following namespaces:")
        pprint(xml.nsmap)
        namespace = xml.nsmap[None]
        try:
            if namespace:
                parser = NoticeParser(xml)
                parsed_notice = parser.parse()
                if parsed_notice:
                    print(f"contract notice successfully parsed.")
                    pprint(parsed_notice)
                else:
                    print(f"Could not parse notice")
        except Exception as err:
            print(f'Error during parsing : {err}')

    print(f"\nContract award notice:")
    with open('../../../data/notices/can.xml') as f:
        content = f.read()
        xml = etree.fromstring(bytes(content, encoding='utf-8'))
        print(f"Extracted the following namespaces:")
        pprint(xml.nsmap)
        namespace = xml.nsmap[None]
        try:
            if namespace:
                parser = NoticeParser(xml)
                parsed_notice = parser.parse()
                if parsed_notice:
                    print(f"contract notice successfully parsed.")
                    pprint(parsed_notice)
                else:
                    print(f"Could not parse notice")
        except Exception as err:
            print(f'Error during parsing : {err}')

    print(f"\nContract award notice:")
    with open('../../../data/notices/438996-2016.xml') as f:
        content = f.read()
        xml = etree.fromstring(bytes(content, encoding='utf-8'))
        print(f"Extracted the following namespaces:")
        pprint(xml.nsmap)
        namespace = xml.nsmap[None]
        try:
            if namespace:
                parser = NoticeParser(xml)
                parsed_notice = parser.parse()
                if parsed_notice:
                    print(f"contract notice successfully parsed.")
                    pprint(parsed_notice)
                else:
                    print(f"Could not parse notice")
        except Exception as err:
            print(f'Error during parsing : {err}')
