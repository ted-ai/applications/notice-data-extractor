import logging
import re
import uuid
from typing import List, Union, Any

import pandas as pd
from lxml import etree
from lxml.etree import _Element

from src.common import utils
from src.common.aws_requests import get_notice_from_s3
from src.common.exceptions import S3UploadException
from src.common.utils import get_env_var_value
from src.lambdas.download_resources.eform_parser import EformNoticeParser
from src.lambdas.download_resources.notice_parser import NoticeParser

S3_KEY_PATTERN = re.compile(r'resource_type=(.*)/format=(.*)/year=(\d*)/month=(\d*)/day=(\d*)/contract_id=(.*)/(.*)')


def get_parser(xml: _Element) -> Union[EformNoticeParser, NoticeParser]:
    logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))
    logger.info(f'Trying to find parser with ns map: {xml.nsmap}')

    if any("http://publications.europa.eu/TED_schema/Export" in value for value in xml.nsmap.values()):
        return NoticeParser(xml)
    if any("ted/" in value for value in xml.nsmap.values()):
        return NoticeParser(xml)
    if any("urn:oasis:names:specification:ubl:schema:xsd:" in value for value in xml.nsmap.values()):
        return EformNoticeParser(xml)

    raise Exception(f"Could not find parser to use for notice: {xml}")


def process_notices(notices: List[str]):
    logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))
    logger.info(f'Start processing {len(notices)} notices.')
    try:
        notice_extract_list = list()
        for notice_s3_info in notices:
            logger.info(f'Processing notice from S3: {notice_s3_info}')
            bucket, s3_key = notice_s3_info.split('#')
            notice_str = get_notice_from_s3(bucket=bucket, key=s3_key)
            notice = process_notice(notice_str, bucket, s3_key)
            notice_extract_list.append(notice)
            logger.info(f'Info extracted from notice: {notice}')
        if notice_extract_list:
            logger.info(f'Writing extracted info from {len(notice_extract_list)} notices to Parquet')
            logger.info(f'Data: {notice_extract_list}')

            df = pd.DataFrame(data=notice_extract_list)
            df = df.replace(r'\n', ' ', regex=True)
            logger.info(f'Dataframe created: {df.head()}')

            curated_bucket = get_env_var_value("CURATED_BUCKET")
            filename = f"{str(uuid.uuid4())}.parquet"
            s3_key = f'data_source=notice/format=parquet/{filename}'
            s3_path = f's3://{curated_bucket}/{s3_key}'
            try:
                logger.info(f"Writing dataframe with {len(notice_extract_list)} elements into {s3_path}")
                df.to_parquet(s3_path, engine="fastparquet")
                logger.info(f"{s3_path} uploaded")
            except Exception as err:
                raise S3UploadException(f"Failed to upload S3 object '{s3_path}': {err}") from err
        else:
            logger.info("No info extracted from notices, nothing to write in Parquet.")
    except Exception as err:
        logger.error(f'Error while downloading and parsing notices : {err}')


def process_notice(notice_str: str, bucket: str, s3_key: str) -> dict[str, Any]:
    logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))
    result = S3_KEY_PATTERN.search(s3_key)
    if result is None:
        raise Exception(f'Could not extract information from path: {s3_key}')
    logger.info(f'Found groups: {result.groups()}')
    notice_type = result.group(1)
    notice_format = result.group(2)
    notice_year = result.group(3)
    notice_month = result.group(4)
    notice_day = result.group(5)
    notice_contract_id = result.group(6)
    notice_filename = result.group(7)
    logger.info(f"Extracted notice info: {notice_type},{notice_format},{notice_year},"
                f"{notice_month},{notice_day},{notice_contract_id},{notice_filename}")
    logger.info('Successfully retrieved notice from S3.')
    xml = etree.fromstring(bytes(notice_str, encoding='utf-8'))
    logger.info('XML notice successfully parsed from string.')

    logger.info(f'Get parser for notice')
    parser = get_parser(xml)

    logger.info(f'NSMAP: {xml.nsmap}')

    logger.info(f'Extracting information from notice with parser {type(parser)}.')
    notice = parser.parse()

    logger.info('Notice parsed. Adding extra information from S3 location partition keys')
    notice["year"] = notice_year
    notice["month"] = notice_month
    notice["day"] = notice_day
    notice["contract_id"] = notice_contract_id
    notice["ingestion_type"] = notice_type
    notice["s3_bucket"] = bucket
    notice["s3_key_path"] = s3_key
    return notice
