import os
from typing import Dict

from src.common.aws_requests import get_ssm_parameter
from src.common.config import SSM_S3_INPUT_BUCKET_ID, SSM_S3_CURATED_BUCKET_ID
from src.common.log import create_logger
from src.common.utils import get_env_var_value
from src.lambdas.download_resources.notice_service import process_notices

os.environ["LOGGER_NAME"] = "download-resources-handler"
os.environ["INPUT_BUCKET"] = get_ssm_parameter(SSM_S3_INPUT_BUCKET_ID)
os.environ["CURATED_BUCKET"] = get_ssm_parameter(SSM_S3_CURATED_BUCKET_ID)
logger = create_logger(get_env_var_value("LOGGER_NAME"))


def handle(_event, _context) -> Dict:
    logger.info(f'Method download_resources triggered with parameters : {_event}')
    messages = _event['Records']
    try:
        notice_ids = [message['body'] for message in messages]
        logger.info(f'Started extracting information from {len(notice_ids)} notices.')
        process_notices(notice_ids)

        return {"statusCode": 200,
                "body": f"{len(notice_ids)} notice(s) processed"}
    except Exception as err:
        raise err
