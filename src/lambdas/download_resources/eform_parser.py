import logging
import os
import re
from dataclasses import asdict
from typing import List

from lxml import etree
from lxml.etree import _Element
from pprint import pprint

from src.common import utils
from src.common.models import AwardedCompany
from src.common.models import BudgetaryValues
from src.common.models import Buyer
from src.common.models import Notice
from src.common.models import NoticeType
from src.common.utils import first_from_multiple_xpath_result
from src.common.utils import first_xpath_result
from src.common.utils import get_field_from_xpath
from src.common.utils import normalize_notice_id, clean_str, is_valid_etendering_url
from src.common.utils import normalize_notice_type
from src.common.utils import run_xpath
from src.common.eform_xpaths import *


class EformNoticeParser:
    xml: _Element
    namespaces: dict

    def __init__(self, xml: _Element):
        self.xml = xml
        ns = get_namespace(xml)
        nsmap = xml.nsmap
        new_nsmap = nsmap | ns
        if None in new_nsmap.keys():
            del new_nsmap[None]
        self.namespaces = new_nsmap
        self.logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))

    def parse(self) -> dict:
        notice_id = self.get_notice_id()
        notice_type = self.get_notice_type().value
        notice_title = self.get_title()
        notice_short_description = self.get_short_description()
        notice_contract_type = self.get_contract_type()
        notice_main_cpv = self.get_main_cpv()
        notice_additional_cpvs = self.get_additional_cpvs()
        if notice_additional_cpvs:
            notice_additional_cpvs = ",".join(notice_additional_cpvs)
        else:
            notice_additional_cpvs = ""
        notice_publication_date = self.get_pub_date()
        notice_reference_notice_id = self.get_ref_notice_id()
        notice_version = self.get_notice_version()
        etendering_url = self.get_etendering_url()
        awarded_companies = []
        if notice_type == NoticeType.CONTRACT_AWARD_NOTICE:
            awarded_companies = self.get_awarded_companies()
        budgetary_values = self.get_budgetary_values()
        buyer = self.get_buyer()
        country = self.get_country()
        is_eu_institution = self.is_eu_institution()
        language = self.get_language()
        notice = Notice(
                id=notice_id,
                type=notice_type,
                title=notice_title,
                short_description=notice_short_description,
                contract_type=notice_contract_type,
                main_cpv=notice_main_cpv,
                additional_cpvs=notice_additional_cpvs,
                publication_date=notice_publication_date,
                reference_notice_id=notice_reference_notice_id,
                version=notice_version,
                etendering_url=etendering_url,
                awarded_companies=awarded_companies,
                budgetary_values=budgetary_values,
                buyer=buyer,
                country=country,
                language=language,
                is_eu_institution=is_eu_institution
        )
        self.logger.info(f'Finished parsing the notice.')
        return asdict(notice)

    def get_notice_id(self) -> str:
        self.logger.info(f"[eForm parser] Retrieving notice ID with XPATH: {NOTICE_ID_XPATH}")
        notice_id = first_xpath_result(self.xml, NOTICE_ID_XPATH, self.namespaces)
        return normalize_notice_id(notice_id)

    def get_pub_date(self) -> str:
        self.logger.info(f"[eForm parser] Retrieving publication date with XPATH: {PUB_DATE_XPATH}")
        date = first_xpath_result(self.xml, PUB_DATE_XPATH, self.namespaces)
        return date

    def get_ref_notice_id(self) -> str:
        self.logger.info(f"[eForm parser] Retrieving reference notice ID with XPATH: {REF_NOTICE_ID_XPATH}")
        ref = first_xpath_result(self.xml, REF_NOTICE_ID_XPATH, self.namespaces)
        return normalize_notice_id(ref)

    def get_main_cpv(self) -> str:
        self.logger.info(f"[eForm parser] Retrieving main CPV with XPATH: {MAIN_CPV_XPATH}")
        cpv = first_from_multiple_xpath_result(self.xml, MAIN_CPV_XPATH, self.namespaces)
        return str(cpv)

    def get_additional_cpvs(self) -> List[str]:
        self.logger.info(f"[eForm parser] Retrieving additional CPVs with XPATH: {ADDITIONAL_CPVS_XPATH}")
        return run_xpath(self.xml, ADDITIONAL_CPVS_XPATH, self.namespaces)

    def get_contract_type(self) -> str:
        self.logger.info(f"[eForm parser] Retrieving contract type with XPATH: {CONTRACT_TYPE_XPATH}")
        contract_type = first_xpath_result(self.xml, CONTRACT_TYPE_XPATH, self.namespaces)
        return str(contract_type)

    def get_notice_version(self) -> str:
        self.logger.info(f"[eForm parser] Retrieving notice version with XPATH: {NOTICE_VERSION_XPATH}")
        version = first_xpath_result(self.xml, NOTICE_VERSION_XPATH, self.namespaces)
        return version

    def get_title(self) -> str:
        self.logger.info(f"[eForm parser] Retrieving title with XPATH: {NOTICE_TITLE_XPATH}")
        title = first_from_multiple_xpath_result(self.xml, NOTICE_TITLE_XPATH, self.namespaces)
        return clean_str(title)

    def get_short_description(self) -> str:
        self.logger.info(f"[eForm parser] Retrieving short description with XPATH: {SHORT_DESC_XPATH}")
        desc = first_from_multiple_xpath_result(self.xml, SHORT_DESC_XPATH, self.namespaces)
        return clean_str(desc)

    def get_notice_type(self) -> NoticeType:
        self.logger.info(f"[eForm parser] Retrieving notice type with XPATH: {NOTICE_TYPE_XPATH}")
        notice_type = first_xpath_result(self.xml, NOTICE_TYPE_XPATH, self.namespaces)
        return normalize_notice_type(notice_type)

    def get_etendering_url(self) -> str:
        self.logger.info(f"[eForm parser] Retrieving etendering URL with XPATH: {ETENDERING_URL_XPATH}")
        url_result = first_xpath_result(self.xml, ETENDERING_URL_XPATH, self.namespaces)
        return url_result if is_valid_etendering_url(url_result) else ''

    def get_awarded_companies(self) -> List[dict]:
        self.logger.info(f"[eForm parser] Retrieving awarded companies with XPATH: {LOTS_XPATH}")
        lots = self.xml.xpath(LOTS_XPATH, namespaces=self.namespaces)
        lot_number = 1
        result = []
        for lot in lots:
            awarded_companies_result = []
            tendering_parties = self.xml.xpath(TENDERING_PARTY_XPATH.format(lot), namespaces=self.namespaces)
            for tendering_party in tendering_parties:
                organization_ids = self.xml.xpath(ORGANIZATION_XPATH.format(tendering_party),
                                                  namespaces=self.namespaces)
                for organization_id in organization_ids:
                    awarded_company = get_awarded_company_info(self.xml, organization_id, self.namespaces)
                    awarded_companies_result.append(awarded_company)
            lot_result = {
                "lot": str(lot_number),
                "awarded": awarded_companies_result
            }
            result.append(lot_result)
            lot_number += 1
        return result

    def get_budgetary_values(self) -> List[dict]:
        self.logger.info(f"[eForm parser] Retrieving budgetary values")
        values = [asdict(BudgetaryValues(
                type="PROCUREMENT",
                lot="",
                estimated_value=first_xpath_result(self.xml, ESTIMATED_VALUE_XPATH, self.namespaces),
                estimated_value_currency=first_xpath_result(self.xml, ESTIMATED_VALUE_CURRENCY_XPATH, self.namespaces),
                min_value=first_xpath_result(self.xml, MIN_VALUE_XPATH, self.namespaces),
                min_value_currency=first_xpath_result(self.xml, MIN_VALUE_CURRENCY_XPATH, self.namespaces),
                max_value=first_xpath_result(self.xml, MAX_VALUE_XPATH, self.namespaces),
                max_value_currency=first_xpath_result(self.xml, MAX_VALUE_CURRENCY_XPATH, self.namespaces),
        ))]
        for lot in run_xpath(self.xml, LOTS_XPATH, self.namespaces):
            clean_lot = next(iter(re.findall(r'\d+', lot)), '').lstrip('0')
            values.append(asdict(BudgetaryValues(
                    type="LOT",
                    lot=clean_lot[0] if isinstance(clean_lot, list) else clean_lot,
                    estimated_value=first_xpath_result(self.xml, LOT_ESTIMATED_VALUE_XPATH.format(lot=lot), self.namespaces),
                    estimated_value_currency=first_xpath_result(self.xml, LOT_ESTIMATED_VALUE_CURRENCY_XPATH.format(lot=lot), self.namespaces),
                    min_value=first_xpath_result(self.xml, LOT_MIN_VALUE_XPATH.format(lot=lot), self.namespaces),
                    min_value_currency=first_xpath_result(self.xml, LOT_MIN_VALUE_CURRENCY_XPATH.format(lot=lot), self.namespaces),
                    max_value=first_xpath_result(self.xml, LOT_MAX_VALUE_XPATH.format(lot=lot), self.namespaces),
                    max_value_currency=first_xpath_result(self.xml, LOT_MAX_VALUE_CURRENCY_XPATH.format(lot=lot), self.namespaces),
            )))
        return values

    def get_buyer(self) -> dict:
        self.logger.info(f"[eForm parser] Retrieving buyer")
        buyer = Buyer(
                name=first_xpath_result(self.xml, BUYER_NAME_XPATH, self.namespaces),
                address=first_xpath_result(self.xml, BUYER_ADDRESS_XPATH, self.namespaces),
                town=first_xpath_result(self.xml, BUYER_TOWN_XPATH, self.namespaces),
                postal_code=first_xpath_result(self.xml, BUYER_POSTAL_CODE_XPATH, self.namespaces)
        )
        return asdict(buyer)

    def get_country(self) -> str:
        self.logger.info(f"[eForm parser] Retrieving country with XPATH: {COUNTRY_ISO_3_XPATH}")
        country = first_xpath_result(self.xml, COUNTRY_ISO_3_XPATH, self.namespaces)
        return country

    def get_language(self) -> str:
        self.logger.info(f"[eForm parser] Retrieving language notice with XPATH: {LANGUAGE_XPATH}")
        language = first_xpath_result(self.xml, LANGUAGE_XPATH, self.namespaces)
        return language

    def is_eu_institution(self) -> bool:
        self.logger.info(f"[eForm parser] Retrieving is_eu_institution with XPATH: {BUYER_TYPE_XPATH}")
        buyer_type = first_xpath_result(self.xml, BUYER_TYPE_XPATH, self.namespaces)
        self.logger.info(f"Buyer type : {buyer_type}")
        return buyer_type == "eu-ins-bod-ag"


def get_namespace(xml: _Element) -> dict:
    logger = None
    try:
        logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))
    except:
        pass
    try:
        if "urn" in xml.nsmap.keys():
            return {'urn': xml.nsmap["urn"]}
        elif None in xml.nsmap.keys():
            return {'urn': xml.nsmap[None]}
        else:
            raise KeyError(f"Unhandled case for XML namespace: {xml.nsmap}")
    except KeyError:
        if logger:
            logger.info('Namespace not found')
        return {}


def get_awarded_company_info(company_xml: _Element, organization_id: str, namespaces: dict) -> dict:
    awarded_company = AwardedCompany(
            get_field_from_xpath(company_xml, namespaces, COMPANY_NAME_XPATH.format(organization_id)),
            get_field_from_xpath(company_xml, namespaces, COMPANY_NATIONAL_ID_XPATH.format(organization_id)),
            get_field_from_xpath(company_xml, namespaces, COMPANY_TOWN_XPATH.format(organization_id)),
            get_field_from_xpath(company_xml, namespaces, COMPANY_ADDRESS_XPATH.format(organization_id)),
            get_field_from_xpath(company_xml, namespaces, COMPANY_POSTAL_CODE_XPATH.format(organization_id)),
            get_field_from_xpath(company_xml, namespaces, COMPANY_COUNTRY_XPATH.format(organization_id)),
            get_field_from_xpath(company_xml, namespaces, COMPANY_PHONE_XPATH.format(organization_id)),
            get_field_from_xpath(company_xml, namespaces, COMPANY_EMAIL_XPATH.format(organization_id)),
            ""  # Not found in the current examples
    )
    return asdict(awarded_company)


if __name__ == '__main__':
    os.environ["LOGGER_NAME"] = "eform_parser"
    print(f"Contract notice eform:")
    with open('../../../data/eforms/contract_notice.xml') as f:
        content = f.read()
        xml = etree.fromstring(bytes(content, encoding='utf-8'))
        print(f"Extracted the following namespaces:")
        pprint(xml.nsmap)
        namespace = get_namespace(xml)
        try:
            if namespace:
                parser = EformNoticeParser(xml)
                parsed_notice = parser.parse()
                if parsed_notice:
                    print(f"eForm notice successfully parsed.")
                    pprint(parsed_notice)
                else:
                    print(f"Could not parse notice")
        except Exception as err:
            print(f'Error during parsing : {err}')

    print(f"\nprior information notice eform:")
    with open('../../../data/eforms/pin.xml') as f:
        content = f.read()
        xml = etree.fromstring(bytes(content, encoding='utf-8'))
        print(f"Extracted the following namespaces:")
        pprint(xml.nsmap)
        namespace = get_namespace(xml)
        try:
            if namespace:
                parser = EformNoticeParser(xml)
                parsed_notice = parser.parse()
                if parsed_notice:
                    print(f"eForm notice successfully parsed.")
                    pprint(parsed_notice)
                else:
                    print(f"Could not parse notice")
        except Exception as err:
            print(f'Error during parsing : {err}')

    print(f"\nContract award notice eform:")
    with open('../../../data/eforms/can.xml') as f:
        content = f.read()
        xml = etree.fromstring(bytes(content, encoding='utf-8'))
        print(f"Extracted the following namespaces:")
        pprint(xml.nsmap)
        namespace = get_namespace(xml)
        try:
            if namespace:
                parser = EformNoticeParser(xml)
                parsed_notice = parser.parse()
                if parsed_notice:
                    print(f"eForm notice successfully parsed.")
                    pprint(parsed_notice)
                else:
                    print(f"Could not parse notice")
        except Exception as err:
            print(f'Error during parsing : {err}')

    print(f"\nprior information notice (change) eform:")
    with open('../../../data/eforms/pin-change.xml') as f:
        content = f.read()
        xml = etree.fromstring(bytes(content, encoding='utf-8'))
        print(f"Extracted the following namespaces:")
        pprint(xml.nsmap)
        namespace = get_namespace(xml)
        try:
            if namespace:
                parser = EformNoticeParser(xml)
                parsed_notice = parser.parse()
                if parsed_notice:
                    print(f"eForm notice successfully parsed.")
                    pprint(parsed_notice)
                else:
                    print(f"Could not parse notice")
        except Exception as err:
            print(f'Error during parsing : {err}')
