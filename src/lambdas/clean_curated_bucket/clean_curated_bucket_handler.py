import os
from typing import Dict

from src.common.aws_requests import get_s3_objects_list, delete_s3_objects
from src.common.aws_requests import get_ssm_parameter
from src.common.config import SSM_S3_CURATED_BUCKET_ID
from src.common.log import create_logger
from src.common.utils import get_env_var_value

os.environ["LOGGER_NAME"] = "clean-curated-bucket-handler"
os.environ["CURATED_BUCKET"] = get_ssm_parameter(SSM_S3_CURATED_BUCKET_ID)
logger = create_logger(get_env_var_value("LOGGER_NAME"))


def handle(_event, _context) -> Dict:
    logger.info(f'Method clean-curated-bucket handle triggered with parameters : {_event}')
    curated_bucket = get_env_var_value("CURATED_BUCKET")
    prefix = "data_source=notice/format=parquet/"
    deleted_count = 0
    try:
        logger.info(f'Get list of objects to delete from bucket {curated_bucket} and prefix {prefix}')
        objects_to_delete_list = get_s3_objects_list(bucket=curated_bucket, prefix=prefix)
        if len(objects_to_delete_list):
            logger.info(f'{len(objects_to_delete_list)} objects to delete.')

            logger.info(f'Started the delete process')
            delete_s3_objects(bucket=curated_bucket, keys=objects_to_delete_list)
            logger.info(f'Done.')
            return {"statusCode": 200, "body": f"{deleted_count} objects from bucket {curated_bucket}."}
        else:
            logger.info(f'Nothing to delete')
            return {"statusCode": 200, "body": f"Bucket already empty"}
    except Exception as err:
        logger.error(f'Error during the cleaning of S3 : {err}')
        raise err
